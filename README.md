Projet cours Spring
== 
Spring Framework version 5

### Installation docker
```
apt update
apt install docker.io
```

### Lancement sur docker.io
```
sudocker build -t airb .
sudo docker image ls
sudo docker run -p 80:8080 airb
```
