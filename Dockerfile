FROM openjdk:8-jdk-alpine

LABEL maintainer="e.martinet0@gmail.com"

VOLUME /tmp

EXPOSE 8080

ARG JAR_FILE=target/airb-0.0.1-SNAPSHOT.jar

ADD ${JAR_FILE} airb.jar

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/airb.jar"]
