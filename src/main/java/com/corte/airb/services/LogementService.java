package com.corte.airb.services;

import com.corte.airb.repository.LogementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LogementService {

    @Autowired
    private LogementRepository logementRepository;

    /*
    public List<Logement> findByDescription(final String description) {
        return logementRepository.findAll()
                .stream()
                .filter(logement -> logement.getDescription().contains(description))
                .collect(Collectors.toList());
    }


    public List<Logement> findInRadius(double latitude, double longitude, double radiusKm) {
        return logementRepository.findAll()
                .stream()
                .filter(logement -> Haversine.distance(logement.getPosition().getLatitude(), logement.getPosition().getLongitude(), latitude, longitude) <= radiusKm)
                .collect(Collectors.toList());
    }

    public List<Logement> findByScoreAbove(Double score) {
        return commentRepository.findAll()
                .stream()
                .filter(comment -> comment.getNote() > score)
                .map(Comment::getLogement)
                .collect(Collectors.toList());

    }
*/

    public static class Haversine {
        private static final int EARTH_RADIUS = 6371;

        static double distance(double startLat, double startLong,
                               double endLat, double endLong) {

            double dLat  = Math.toRadians((endLat - startLat));
            double dLong = Math.toRadians((endLong - startLong));

            startLat = Math.toRadians(startLat);
            endLat   = Math.toRadians(endLat);

            double a = haversin(dLat) + Math.cos(startLat) * Math.cos(endLat) * haversin(dLong);
            double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

            return EARTH_RADIUS * c;
        }

        static double haversin(double val) {
            return Math.pow(Math.sin(val / 2), 2);
        }
    }
}
