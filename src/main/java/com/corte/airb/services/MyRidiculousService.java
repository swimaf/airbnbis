package com.corte.airb.services;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile("ridiculuse")
public class MyRidiculousService implements MyService {

    @Override
    public String getText() {
        return "Ridiculus service";
    }
}
