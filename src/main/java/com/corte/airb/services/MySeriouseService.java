package com.corte.airb.services;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile("default")
public class MySeriouseService implements MyService {

    @Override
    public String getText() {
        return "Seriouse service";
    }
}
