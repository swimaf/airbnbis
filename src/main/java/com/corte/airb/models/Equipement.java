package com.corte.airb.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Equipement implements Model<Long> {

    @NotNull
    private Long id;

    @Size(max = 100)
    private String nom;
    private Boolean valeur;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Boolean getValeur() {
        return valeur;
    }

    public void setValeur(Boolean valeur) {
        this.valeur = valeur;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
