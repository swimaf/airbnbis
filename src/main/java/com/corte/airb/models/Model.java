package com.corte.airb.models;

public interface Model<K> {
    K getId();
}
