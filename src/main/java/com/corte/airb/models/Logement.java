package com.corte.airb.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.util.List;

@Document(indexName = "logements", type = "logement")
public class Logement {

    @Id
    @NotNull
    private Long id;

    private Double price;
    private String type;
    private Integer maxOccupation;
    private Integer surface;
    private Integer nbRoom;
    private String description;
    private Position position;
    private Boolean disponible;
    private List<String> photos;

    private List<Equipement> equipements;
    private List<Comment> comments;
    private User contact;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getMaxOccupation() {
        return maxOccupation;
    }

    public void setMaxOccupation(Integer maxOccupation) {
        this.maxOccupation = maxOccupation;
    }

    public Integer getSurface() {
        return surface;
    }

    public void setSurface(Integer surface) {
        this.surface = surface;
    }

    public Integer getNbRoom() {
        return nbRoom;
    }

    public void setNbRoom(Integer nbRoom) {
        this.nbRoom = nbRoom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Boolean getDisponible() {
        return disponible;
    }

    public void setDisponible(Boolean disponible) {
        this.disponible = disponible;
    }

    public List<String> getPhotos() {
        return photos;
    }

    public void setPhotos(List<String> photos) {
        this.photos = photos;
    }

    public List<Equipement> getEquipements() {
        return equipements;
    }

    public void setEquipement(List<Equipement> equipements) {
        this.equipements = equipements;
    }

    public User getContact() {
        return contact;
    }

    public void setContact(User contact) {
        this.contact = contact;
    }

    @Override
    public String toString() {
        return "Logement{" +
                "id=" + id +
                ", price=" + price +
                ", type='" + type + '\'' +
                ", maxOccupation=" + maxOccupation +
                ", surface=" + surface +
                ", nbRoom=" + nbRoom +
                ", description='" + description + '\'' +
                ", position=" + position +
                ", disponible=" + disponible +
                ", photos=" + photos +
                ", equipements=" + equipements +
                ", contact=" + contact +
                '}';
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }
}
