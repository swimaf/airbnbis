package com.corte.airb.repository;


import com.corte.airb.models.Model;

import java.util.List;

public interface CrudRepository<K,V extends Model<K>> {
    List<V> findAll();
    V findById(K id);
    V saveOrUpdate(V entity);
    V deleteById(K id);
}
