package com.corte.airb.repository;


import com.corte.airb.models.Logement;
import org.springframework.context.annotation.Scope;
import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;
import org.springframework.stereotype.Repository;

public interface LogementRepository extends ElasticsearchCrudRepository<Logement, Long> {

}
