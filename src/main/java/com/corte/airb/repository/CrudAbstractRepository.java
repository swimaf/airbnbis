package com.corte.airb.repository;


import com.corte.airb.models.Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

abstract public class CrudAbstractRepository<K, V extends Model<K>> implements CrudRepository<K, V>{

    private HashMap<K, V> database = new HashMap<>();

    public List<V> findAll() {
        return new ArrayList<>(database.values());
    }

    public V findById(K id) {
        return database.get(id);
    }

    public V saveOrUpdate(V entity) {
        database.put(entity.getId(), entity);
        return entity;
    }

    public V deleteById(K id) {
        return database.remove(id);
    }

}
