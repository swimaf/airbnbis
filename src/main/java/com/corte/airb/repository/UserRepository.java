package com.corte.airb.repository;


import com.corte.airb.models.User;
import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;

import java.util.List;

public interface UserRepository extends ElasticsearchCrudRepository<User, Long> {
    List<User> findAllByFullName(String fullName);
    List<User> findAllByEmail(String email);
}
