package com.corte.airb.controller;

import com.corte.airb.models.Model;
import com.corte.airb.repository.CrudAbstractRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Validated
public abstract class AbstractController<T, K extends Serializable> {

    protected ElasticsearchCrudRepository<T, K> repository;

    @Autowired
    public AbstractController(ElasticsearchCrudRepository<T, K> repository) {
        this.repository = repository;
    }

    @GetMapping
    public List<T> all() {
        List<T> list = new ArrayList<>();
        repository.findAll().forEach(list::add);
        return list;
    }

    @PostMapping
    public T newModel(@RequestBody @Valid @NotNull T entity) {
        return repository.save(entity);
    }

    @GetMapping("/{id}")
    public T one(@PathVariable K id) {
        return repository.findById(id).get();
    }

}

