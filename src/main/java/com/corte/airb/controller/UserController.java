package com.corte.airb.controller;

import com.corte.airb.models.User;
import com.corte.airb.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/users")
public class UserController extends AbstractController<User, Long>{

    @Autowired
    public UserController(UserRepository repository) {
        super(repository);
    }


}
