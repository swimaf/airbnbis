package com.corte.airb.controller;

import com.corte.airb.models.Logement;
import com.corte.airb.repository.LogementRepository;
import com.corte.airb.services.LogementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/logements")
public class LogementController {

    //private final LogementService logementService;

    @Autowired
    public LogementController(LogementRepository repository) {
        this.repository =repository;
        //this.logementService = logementService;
    }


    protected LogementRepository repository;


    @GetMapping
    public List<Logement> all() {
        List<Logement> list = new ArrayList<>();
        repository.findAll().forEach(list::add);
        return list;
    }

    @PostMapping
    public Logement newModel(@RequestBody @Valid @NotNull Logement entity) {
        return repository.save(entity);
    }

    @GetMapping("/{id}")
    public Logement one(@PathVariable Long id) {
        return repository.findById(id).get();
    }
    /*
    @GetMapping("/logements/filter/description/{desc}")
    public List<Logement> allByDesc(@RequestParam(name="desc") String desc) {
        return logementService.findByDescription(desc);
    }*/

}
